<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Leads extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("leads_model");
        $this->load->model("customers_model");
        $this->load->model("staff_model");
        $this->load->model("salesteams_model");
        $this->load->model("calls_model");
        $this->load->library('form_validation');
         
        /*cache control*/
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
         
        check_login();
    }

    public function index()
    {
        //checking permission for staff
        if (!check_staff_permission('lead_read')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
            
            
        $data['leads'] = $this->leads_model->leads_list(userdata('id'));
                                 
        $this->load->view('header');
        $this->load->view('leads/index', $data);
        $this->load->view('footer');
    }
    public function add()
    {
        //checking permission for staff
        if (!check_staff_permission('lead_write')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
             
        
        $data['cities'] = $this->leads_model->city_list_all();
                
        
                 
        $this->load->view('header');
        $this->load->view('leads/add', $data);
        $this->load->view('footer');
    }


    public function import()
    {
        //checking permission for staff
        if (!check_staff_permission('lead_write')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
             
        
        
                
        
                 
        $this->load->view('header');
        $this->load->view('leads/import', $data);
        $this->load->view('footer');
    }


    public function import_process()
    {
        if (!check_staff_permission('lead_write')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }

        $this->form_validation->set_rules('lead_source', 'Lead Source', 'required');
        $this->form_validation->set_rules('csv_file', 'CSV File', 'required');

       
        $dataType=$this->input->post('lead_source');
      
        if ($_FILES) {
            $file=$_FILES['csv_file']['tmp_name'];
            if ($file == null) {
                echo '<div class="alert error"><ul><li style="color:red">CSV File required</li></ul></div>';
            } else {
                $handle = fopen($file, "r") or die("err");
                switch ($dataType) {
                   
                    case 'Facebook':
                      
                    $row=0;
                    while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                        if ($row++ < 1) {
                            continue;
                        }
                        list($a, $b, $c)=explode(' ', $filesop[6]);
                        if ($a != 'Franchise') {
                            continue;
                        }
                        $data['lead_date'] = date('Y-m-d', strtotime($filesop[0]));
                        $data['contact_name'] = $filesop[1];
                        $data['email'] = $filesop[2];
                        $data['phone'] = $filesop[3];
                        $city=$this->leads_model->get_city_id($c);
                        $data['city_id'] =$city->id;
                        
                        $data['lead_source'] = "Online";
                        $data['lead_upload_source'] = "Facebook";
    
                        $this->leads_model->add_lead_upload($data);
                        //print_r($this->db->last_query());
                        $lead_id=$this->db->insert_id();
                        //print_r($data);
                        //echo 'yes_'.$lead_id;
                        //echo $this->lang->line('technical_problem');
                    }

                     echo "Upload success";
                    break;
                    case 'Google':
                        echo $this->lang->line('technical_problem');
                    
                        break;
                }
            }
        }
    }


    public function add_process()
    {
                  
        //checking permission for staff
        if (!check_staff_permission('lead_write')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
        
       
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|regex_match[/^[0-9]{10}$/]');
       
                 
        
        if ($this->form_validation->run() == false) {
            echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">', '</li>') . '</ul></div>';
        } elseif ($this->leads_model->exists_email($this->input->post('email')) > 0) {
            echo '<div class="alert error">Email already used.</div>';
        } else {
            if ($this->leads_model->add_leads()) {
                 
               //redirect("admin/leads/update/".$this->db->insert_id());
                // echo '<div class="alert alert-success">'.$this->lang->line('create_succesful').'</div>';
                $lead_id=$this->db->insert_id();
              
                add_notifications($this->input->post('salesperson_id'), 'New Lead Added', $lead_id, 'leads');
             
                echo 'yes_'.$lead_id;
            } else {
                echo $this->lang->line('technical_problem');
            }
        }
    }
    
    public function view($lead_id)
    {
        //checking permission for staff
        if (!check_staff_permission('lead_read')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
                 
                
        $data['companies'] = $this->customers_model->company_list();
        $data['staffs'] = $this->staff_model->staff_list();
        $data['calls'] = $this->calls_model->calls_list($lead_id, $type='leads');
        $data['salesteams'] = $this->salesteams_model->salesteams_list();
                  
        $data['lead'] = $this->leads_model->get_lead($lead_id, userdata('id'));
                 
        $this->load->view('header');
        $this->load->view('leads/view', $data);
        $this->load->view('footer');
    }
    
    public function update($lead_id)
    {
        //checking permission for staff
        if (!check_staff_permission('lead_write')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
                
                 
                
        $data['companies'] = $this->customers_model->company_list();
        $data['countries'] = $this->leads_model->country_list();
        $data['staffs'] = $this->staff_model->staff_list();
        $data['salesteams'] = $this->salesteams_model->salesteams_list();
                
        $data['calls'] = $this->calls_model->calls_list($lead_id, $type='leads');
                
        $data['lead'] = $this->leads_model->get_lead($lead_id, userdata('id'));
                  
                 
        $this->load->view('header');
        $this->load->view('leads/update', $data);
        $this->load->view('footer');
    }
    
    public function update_process()
    {
        //checking permission for staff
        if (!check_staff_permission('lead_write')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
        
       
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|regex_match[/^[0-9]{10}$/]');
        
        if ($this->form_validation->run() == false) {
            echo '<div class="alert alert-danger"><ul>' . validation_errors('<li>', '</li>') . '</ul></div>';
        } else {
            if ($this->leads_model->update_leads()) {
                echo '<div class="alert alert-success">'.$this->lang->line('update_succesful').'</div>';
            } else {
                echo $this->lang->line('technical_problem');
            }
        }
    }
    
    /*
     * deletes lead     *
     */
    public function delete($lead_id)
    {
        //checking permission for staff
        if (!check_staff_permission('lead_delete')) {
            redirect(base_url('admin/access_denied'), 'refresh');
        }
                 
        if ($this->leads_model->delete($lead_id)) {
            echo 'deleted';
        }
    }
    
    public function ajax_state_list($country_id)
    {
        $data['state'] = $this->leads_model->state_list($country_id);
        $this->load->view('ajax_get_state', $data);
    }
    public function ajax_city_list($state_id)
    {
        $data['cities'] = $this->leads_model->city_list($state_id);
        $this->load->view('ajax_get_city', $data);
    }
    
    
    //Add Call
    public function add_call()
    {
        $this->form_validation->set_rules('date', 'Date', 'required');
        
        $this->form_validation->set_rules('call_summary', 'Call Summary', 'required');
         
        
        if ($this->form_validation->run() == false) {
            echo '<div style="color:red;margin-left:15px;">' . validation_errors() . '</div>';
        } else {
            if ($this->calls_model->add_calls()) {
                echo 'yes';
            //echo '<div class="alert alert-success">'.$this->lang->line('create_succesful').'</div>';
            } else {
                echo $this->lang->line('technical_problem');
            }
        }
    }
    
    /*
     * deletes call     *
     */
    public function call_delete($call_id)
    {
        check_login();
         
        if ($this->calls_model->delete($call_id)) {
            echo 'deleted';
        }
    }
    
    
    public function edit_call($call_id)
    {
        $data['companies'] = $this->customers_model->company_list();
                
        $data['staffs'] = $this->staff_model->staff_list();
    
        $data['call'] = $this->calls_model->get_call($call_id);
     
          
          
        $this->load->view('header');
        $this->load->view('opportunities/edit_call', $data);
        $this->load->view('footer');
    }
    
    public function edit_call_process()
    {
        $this->form_validation->set_rules('date', 'Date', 'required');
        
        $this->form_validation->set_rules('call_summary', 'Call Summary', 'required');
         
        
        if ($this->form_validation->run() == false) {
            echo '<div style="color:red;margin-left:15px;">' . validation_errors() . '</div>';
        } else {
            if ($this->calls_model->edit_calls()) {
                echo '<div style="margin-left:15px;">'.$this->lang->line('update_succesful').'</div>';
            } else {
                echo $this->lang->line('technical_problem');
            }
        }
    }
    
    public function convert_to_opportunity()
    {
        if ($this->leads_model->add_convert_to_opportunity()) {
                 
              // redirect("admin/opportunities/update/".$this->db->insert_id());
            echo 'yes_'.$this->db->insert_id();
        } else {
            echo $this->lang->line('technical_problem');
        }
    }
}
