<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->

<script type="text/javascript">
$(function(){
    $('.marquee').marquee({
    showSpeed:1000, //speed of drop down animation
    scrollSpeed: 10, //lower is faster
    yScroll: 'bottom',  // scroll direction on y-axis 'top' for down or 'bottom' for up
    direction: 'left', //scroll direction 'left' or 'right'
    pauseSpeed: 1000, // pause before scroll start in milliseconds
    duplicated: true  //continuous true or false
    });
});
</script> 
	        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content page-thin">
           <div class="row">
           	<div class=	"widget-infobox" style="padding-bottom:15px; padding-left:6px;">
                            <div class="row"></div>
                            <h2><b style="text-transform: uppercase;"><?php echo config('site_name'); ?></b> <b>DASHBOARD</b></h2> 
                            <a href="#">
                      		<div class="infobox"> 
                                <div class="left"> 
                                    <i class="fa fa-user bg-orange"></i> 
                                </div>                                 
                                <div class="right"> 
                                    <div> 
                                        <span class="c-orange pull-left"><?php echo $leads;?></span> 
                                        <br> 
                                    </div>                                     
                                    <div class="txt">LEADS</div>                                     
                                </div>                                 
                            </div> 
							</a>
							
						
							
						
							
							<a href="#"> 
                            <div class="infobox"> 
                                <div class="left"> 
                                    <i class="fa fa-phone bg-red"></i> 
                                </div>                                 
                                <div class="right"> 
                                    <div class="clearfix"> 
                                        <div> 
                                            <span class="c-red pull-left"><?php echo $calls;?></span> 
                                            <br> 
                                        </div>                                         
                                        <div class="txt">CALLS</div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>
							</a>
							
							<a href="#"> 
                            <div class="infobox"> 
                                <div class="left"> 
                                    <i class="icon-user bg-yellow"></i> 
                                </div>                                 
                                <div class="right"> 
                                    <div class="clearfix"> 
                                        <div> 
                                            <span class="c-yellow pull-left"><?php echo $meetings;?></span> 
                                            <br> 
                                        </div>                                         
                                        <div class="txt">MEETINGS</div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>
							</a>
					
			
            </div>                
            
          </div>
         
         

         
                    
                    <div class="row">
                    <div class="col-md-4 col-sm-6"> 
                            <div class="panel"> 
                                <div class="panel-header"> 
                                    <h3><i class="icon-list"></i> <strong>LEADS</strong>  </h3> 
                                </div>                                 
                                <div class="panel-content"> 
                                     
                                    <div class="tab-content" style="border: none;min-height: 445px;">
                                    <div class="tab-pane fade active in" id="tab6_cl1">
                                    <ul class="todo-list">  
                                 <?php if (! empty($staff_call_log_list)) {?>
					   			 <?php foreach ($staff_call_log_list as $staff_call_log) { ?>
                                        <li> 
                                             
                                            <span class=""><a href="<?php echo base_url('admin/logged_calls/update/'.$staff_call_log->id); ?>"><?php echo date('m/d/Y', $staff_call_log->date); ?></a></span> 
                                            <div class="todo-date clearfix">  
                                                <div class=""><?php echo customer_name($staff_call_log->company_id)->name; ?>                        
                                                </div>                                                 
                                            </div>                                             
                                              
                                        </li>                                                                                				 <?php } ?>
								 <?php } ?>
                                    </ul>                                     			</div>
									
									 
									</div>
                                    <div class="clearfix m-t-10"> 
                                        <div class="pull-right"> 
                                            <a href="#" class="btn btn-sm btn-dark add-task">OPEN CALLS LOG</a>                                             
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>
                             
                        </div>
						
						
						<div class="col-md-4 col-sm-6"> 
                            <div class="panel"> 
                                <div class="panel-header"> 
                                    <h3><i class="icon-list"></i> <strong>CALLS</strong> LOG </h3> 
                                </div>                                 
                                <div class="panel-content"> 
                                     
                                    <div class="tab-content" style="border: none;min-height: 445px;">
                                    <div class="tab-pane fade active in" id="tab6_cl1">
                                    <ul class="todo-list">  
                                 <?php if (! empty($staff_call_log_list)) {?>
					   			 <?php foreach ($staff_call_log_list as $staff_call_log) { ?>
                                        <li> 
                                             
                                            <span class=""><a href="<?php echo base_url('admin/logged_calls/update/'.$staff_call_log->id); ?>"><?php echo date('m/d/Y', $staff_call_log->date); ?></a></span> 
                                            <div class="todo-date clearfix">  
                                                <div class=""><?php echo customer_name($staff_call_log->company_id)->name; ?>                        
                                                </div>                                                 
                                            </div>                                             
                                              
                                        </li>                                                                                				 <?php } ?>
								 <?php } ?>
                                    </ul>                                     			</div>
									
									 
									</div>
                                    <div class="clearfix m-t-10"> 
                                        <div class="pull-right"> 
                                            <a href="#" class="btn btn-sm btn-dark add-task">OPEN CALLS LOG</a>                                             
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>
                             
                        </div>
						
					
                        <div class="col-md-4 col-sm-6"> 
                            <div class="panel"> 
                                <div class="panel-header"> 
                                    <h3><i class="icon-list"></i> <strong>MEETINGS</strong>  </h3> 
                                </div>                                 
                                <div class="panel-content"> 
                                     
                                    <div class="tab-content" style="border: none;min-height: 445px;">
                                    <div class="tab-pane fade active in" id="tab6_cl1">
                                    <ul class="todo-list">  
                                 <?php if (! empty($staff_call_log_list)) {?>
					   			 <?php foreach ($staff_call_log_list as $staff_call_log) { ?>
                                        <li> 
                                             
                                            <span class=""><a href="<?php echo base_url('admin/logged_calls/update/'.$staff_call_log->id); ?>"><?php echo date('m/d/Y', $staff_call_log->date); ?></a></span> 
                                            <div class="todo-date clearfix">  
                                                <div class=""><?php echo customer_name($staff_call_log->company_id)->name; ?>                        
                                                </div>                                                 
                                            </div>                                             
                                              
                                        </li>                                                                                				 <?php } ?>
								 <?php } ?>
                                    </ul>                                     			</div>
									
									 
									</div>
                                    <div class="clearfix m-t-10"> 
                                        <div class="pull-right"> 
                                            <a href="#" class="btn btn-sm btn-dark add-task">OPEN CALLS LOG</a>                                             
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>
                             
                        </div>
						
						
                    </div>   
         </div>          
         
         
        <!-- END PAGE CONTENT -->
      