<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>

$(document).ready(function() {
	$("form[name='import_leads']").submit(function(e) {
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: "<?php echo base_url('admin/leads/import_process'); ?>",
            type: "POST",
            data: formData,
            async: false,
            success: function (msg) {
			$('body,html').animate({ scrollTop: 0 }, 200);
      $("#leads_ajax").html('<div class="alert alert-success">'+msg+'</div>');
			setTimeout(function () {
				window.location.href="<?php echo base_url('admin/leads'); ?>";
				}, 1000); //will call the function after 2 secs.
			
			  
        },
            cache: false,
            contentType: false,
            processData: false
        });

        e.preventDefault();
    });
});

</script>


 <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
         <div class="header">
            <h2><strong>Import Leads</strong></h2>            
          </div>
           <div class="row">
           	<div class="col-md-12">
                  <div class="panel">
                     
                     <div class="panel-content">
                   					<div id="leads_ajax"> 
				                          <?php if ($this->session->flashdata('message')) {
    echo $this->session->flashdata('message');
}?>         
				                      </div>
				         
				            <form id="import_leads" name="import_leads" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
 
                        				                        				 
							<div class="row">

							<div class="col-sm-6">
					                            <div class="form-group">
					                              <label class="control-label">Lead Source</label>
					                              <div class="append-icon">
                                                  <?php $options = array(
                                                        'Google'  => 'Google',
                                                      'Facebook'  => 'Facebook',
                                                     
                                                    );
                                                    echo form_dropdown('lead_source', $options, 'Google', 'class="form-control"');?>	
					                                
					                              </div>
					                            </div>
					                          </div>
                          					<div class="col-sm-6">
					                            <div class="form-group">
					                              <label class="control-label">Choose File</label>
					                              <div class="append-icon">
					                                <input type="file" name="csv_file" value="" class="form-control">
					                                
					                              </div>
					                            </div>
					                          </div>
					                         
					                         
					                        </div>
							
					                    
                        			
				                       
					                                  
                        				<div class="text-left  m-t-20">
                         				 <div id="leads_submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Import</button></div>
                           
                        </div>
                      </form>             
                  				    
                  </div>
                  </div>
                </div>
           	</div>
            	
 		</div>   
  <!-- END PAGE CONTENT -->
 



