<div class="sidebar">
    <div class="logopanel text-center">
        <a href="<?php echo base_url('admin/dashboard/'); ?>"><img
                src="<?php echo base_url('uploads/site').'/'.config('site_logo'); ?>" alt="company logo" class=""
                style="height: 30px;"></a>

    </div>
    <div class="sidebar-inner">

        <div class="menu-title">
            Navigation
            <!--<div class="pull-right menu-settings">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300"> 
              <i class="icon-settings"></i>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#" id="reorder-menu" class="reorder-menu">Reorder menu</a></li>
                <li><a href="#" id="remove-menu" class="remove-menu">Remove elements</a></li>
                <li><a href="#" id="hide-top-sidebar" class="hide-top-sidebar">Hide user &amp; search</a></li>
              </ul>
            </div>-->
        </div>


        <ul class="nav nav-sidebar">
            <li class=" nav-active <?php echo is_active_menu('dashboard'); ?>"><a
                    href="<?php echo base_url('admin/dashboard'); ?>"><i
                        class="icon-home"></i><span>Dashboard</span></a></li>
            <?php if (check_staff_permission('sales_team_read')) {?>
            <li class=" nav-active <?php echo is_active_menu('salesteams'); ?>"><a
                    href="<?php echo base_url('admin/salesteams'); ?>"><i class="icon-users"></i><span>Sales
                        Teams</span></a></li>
            <?php }?>

            <?php if (check_staff_permission('lead_read')) {?>
            <li class=" nav-active <?php echo is_active_menu('leads'); ?>"><a
                    href="<?php echo base_url('admin/leads'); ?>"><i class="icon-rocket"></i><span>Leads</span></a></li>
            <?php }?>



            <?php if (check_staff_permission('logged_calls_read')) {?>
            <li class=" nav-active <?php echo is_active_menu('logged_calls'); ?>"><a
                    href="<?php echo base_url('admin/logged_calls'); ?>"><i class="fa fa-phone"></i><span>Logged
                        Calls</span></a></li>
            <?php }?>

            <?php if (check_staff_permission('meetings_read')) {?>
            <li class=" nav-active <?php echo is_active_menu('meetings'); ?>"><a
                    href="<?php echo base_url('admin/meetings'); ?>"><i class="fa fa-user"></i><span>Meetings</span></a>
            </li>
            <?php }?>










            <?php if (check_staff_permission('staff_read')) {?>
            <li class=" nav-active <?php echo is_active_menu('staff'); ?>"><a
                    href="<?php echo base_url('admin/staff'); ?>"><i class="icon-users"></i><span>Staff</span></a></li>
            <?php }?>

        </ul>



    </div>
</div>